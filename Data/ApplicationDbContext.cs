﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using GAP.InsuranceTest.Core.Entities;
using GAP.InsuranceTest.Data.Config;
using GAP.InsuranceTest.Models;
using Microsoft.AspNetCore.Identity;

namespace GAP.InsuranceTest.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Insurance> Insurances { get; set; }
        public DbSet<Coverage> Coverages { get; set; }
        public DbSet<Beneficiary> Beneficiaries { get; set; }
        public DbSet<BeneficiaryInsurance> BeneficiaryInsurances { get; set; }
        public DbSet<CoverageInsurance> CoverageInsurances { get; set; }
        public DbSet<InsuranceRiskType> InsuranceRiskTypes { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            // many to many relationships
            builder.Entity<CoverageInsurance>()
                .HasKey(x => new { x.coverageId, x.insuranceId });

            builder.Entity<BeneficiaryInsurance>()
             .HasKey(x => new { x.beneficiaryId, x.insuranceId });

            // Seed coverage table
            builder.Entity<Coverage>().HasData(
                new Coverage { id = 1, name = "Terremoto", enabled = true },
                new Coverage { id = 2, name = "Incendio", enabled = true },
                new Coverage { id = 3, name = "Robo", enabled = true },
                new Coverage { id = 4, name = "Pérdida", enabled = true }
            );

            // seed insurance table
            builder.Entity<InsuranceRiskType>().HasData(
                new InsuranceRiskType { id=1, risk_type="Bajo" },
                new InsuranceRiskType { id = 2, risk_type = "Medio" },
                new InsuranceRiskType { id=3, risk_type="Medio-alto" },
                new InsuranceRiskType { id=4, risk_type= "Alto" }
            );

            //
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new AdminConfiguration());
            builder.ApplyConfiguration(new EmployeeConfiguration());
            builder.ApplyConfiguration(new UsersConfig());
        }
    }
}
