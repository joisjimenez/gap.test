﻿using GAP.InsuranceTest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.InsuranceTest.Data.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Insurance> Insurances { get; }
        IRepository<Beneficiary> Beneficiaries { get; }
        IRepository<Coverage> Coverages { get; }
        IRepository<BeneficiaryInsurance> BeneficiaryInsurances { get; }
        IRepository<CoverageInsurance> CoverageInsurances { get; }
        IRepository<InsuranceRiskType> InsuranceRiskTypes { get; }
        void Commit();
    }
}
