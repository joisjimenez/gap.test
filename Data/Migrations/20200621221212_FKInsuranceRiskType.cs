﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class FKInsuranceRiskType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_insuranceRiskTypeid",
                table: "Insurances");

            migrationBuilder.AlterColumn<int>(
                name: "insuranceRiskTypeid",
                table: "Insurances",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "66d2265b-6c54-406b-b78c-f30dcb94de21");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "039bca01-4ef4-458d-8689-b794da4dd14b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "1c1d3a92-8612-4642-a9e7-04e88798577f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "367e6c38-56aa-4fda-9dc0-4a5f2eab9c6e", "AQAAAAEAACcQAAAAEMG6jplLciaMDrOjnMetYTpBzTyKCPC5olWetAKmV6MRPhf4Su4VhQ51tF6tgl24gg==" });

            migrationBuilder.AddForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_insuranceRiskTypeid",
                table: "Insurances",
                column: "insuranceRiskTypeid",
                principalTable: "InsuranceRiskTypes",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_insuranceRiskTypeid",
                table: "Insurances");

            migrationBuilder.AlterColumn<int>(
                name: "insuranceRiskTypeid",
                table: "Insurances",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "c1f98ef7-aa8b-4347-b3f4-b992dc7c6768");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "40b2009e-a609-4c1a-82e9-21e3fa692c48");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "e78ba8fc-652c-441b-8498-e6abe1781a0f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "8569267e-f1e8-4e9b-986d-e907a9f86089", "AQAAAAEAACcQAAAAEDTaTqWxdkPv8uHRCR2WlmkQ7yKUdX0yj6QSEO9ZuiHkLqDk6u5zIXzYGX+06mx20g==" });

            migrationBuilder.AddForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_insuranceRiskTypeid",
                table: "Insurances",
                column: "insuranceRiskTypeid",
                principalTable: "InsuranceRiskTypes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
