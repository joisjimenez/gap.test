﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class TableInsuranceRiskTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Insurances_InsuranceTypes_unsuranceTypeid",
                table: "Insurances");

            migrationBuilder.DropTable(
                name: "InsuranceTypes");

            migrationBuilder.CreateTable(
                name: "InsuranceRiskTypes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    risk_type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuranceRiskTypes", x => x.id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "25d7c0cb-4768-4176-825a-4d6a5f72588d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "2003e9d4-4239-4870-a611-0c49fe8f2fa5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "33412dd8-5a97-41cc-8d62-d8498a9aabce");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0af46783-6d7d-4b10-9a27-25ee74c6c9b0", "AQAAAAEAACcQAAAAEEWU29zPJWziOoUicvP4Tu1Mszx8fzxM6EHS4taZdTPqXitHfZddVtj8ilXlcGAGSQ==" });

            migrationBuilder.InsertData(
                table: "InsuranceRiskTypes",
                columns: new[] { "id", "risk_type" },
                values: new object[,]
                {
                    { 1, "Bajo" },
                    { 2, "Medio" },
                    { 3, "Medio-alto" },
                    { 4, "Alto" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_unsuranceTypeid",
                table: "Insurances",
                column: "unsuranceTypeid",
                principalTable: "InsuranceRiskTypes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_unsuranceTypeid",
                table: "Insurances");

            migrationBuilder.DropTable(
                name: "InsuranceRiskTypes");

            migrationBuilder.CreateTable(
                name: "InsuranceTypes",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    risk_type = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuranceTypes", x => x.id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "5b11cfd0-a9aa-430d-882a-44eb655e187b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "3229d541-5d20-474c-a771-db52f5323267");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "ad8aaba4-5956-4dda-87ec-181cc4f22b7b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "cc828707-b922-4be6-b65e-6813ce08eb8a", "AQAAAAEAACcQAAAAEPfmE+q1hbMbNm3DkmSPROBanCJYkTTTqGl/iEEO7XsiAoWZXX9YeWO8VPpgdjs/cA==" });

            migrationBuilder.InsertData(
                table: "InsuranceTypes",
                columns: new[] { "id", "risk_type" },
                values: new object[,]
                {
                    { 1, "Bajo" },
                    { 2, "Medio" },
                    { 3, "Medio-alto" },
                    { 4, "Alto" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Insurances_InsuranceTypes_unsuranceTypeid",
                table: "Insurances",
                column: "unsuranceTypeid",
                principalTable: "InsuranceTypes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
