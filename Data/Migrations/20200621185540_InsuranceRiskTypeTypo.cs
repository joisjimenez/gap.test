﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class InsuranceRiskTypeTypo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_unsuranceTypeid",
                table: "Insurances");

            migrationBuilder.DropIndex(
                name: "IX_Insurances_unsuranceTypeid",
                table: "Insurances");

            migrationBuilder.DropColumn(
                name: "unsuranceTypeid",
                table: "Insurances");

            migrationBuilder.AddColumn<int>(
                name: "insuranceRiskTypeid",
                table: "Insurances",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "c1f98ef7-aa8b-4347-b3f4-b992dc7c6768");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "40b2009e-a609-4c1a-82e9-21e3fa692c48");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "e78ba8fc-652c-441b-8498-e6abe1781a0f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "8569267e-f1e8-4e9b-986d-e907a9f86089", "AQAAAAEAACcQAAAAEDTaTqWxdkPv8uHRCR2WlmkQ7yKUdX0yj6QSEO9ZuiHkLqDk6u5zIXzYGX+06mx20g==" });

            migrationBuilder.CreateIndex(
                name: "IX_Insurances_insuranceRiskTypeid",
                table: "Insurances",
                column: "insuranceRiskTypeid");

            migrationBuilder.AddForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_insuranceRiskTypeid",
                table: "Insurances",
                column: "insuranceRiskTypeid",
                principalTable: "InsuranceRiskTypes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_insuranceRiskTypeid",
                table: "Insurances");

            migrationBuilder.DropIndex(
                name: "IX_Insurances_insuranceRiskTypeid",
                table: "Insurances");

            migrationBuilder.DropColumn(
                name: "insuranceRiskTypeid",
                table: "Insurances");

            migrationBuilder.AddColumn<int>(
                name: "unsuranceTypeid",
                table: "Insurances",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "25d7c0cb-4768-4176-825a-4d6a5f72588d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "2003e9d4-4239-4870-a611-0c49fe8f2fa5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "33412dd8-5a97-41cc-8d62-d8498a9aabce");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "0af46783-6d7d-4b10-9a27-25ee74c6c9b0", "AQAAAAEAACcQAAAAEEWU29zPJWziOoUicvP4Tu1Mszx8fzxM6EHS4taZdTPqXitHfZddVtj8ilXlcGAGSQ==" });

            migrationBuilder.CreateIndex(
                name: "IX_Insurances_unsuranceTypeid",
                table: "Insurances",
                column: "unsuranceTypeid");

            migrationBuilder.AddForeignKey(
                name: "FK_Insurances_InsuranceRiskTypes_unsuranceTypeid",
                table: "Insurances",
                column: "unsuranceTypeid",
                principalTable: "InsuranceRiskTypes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
