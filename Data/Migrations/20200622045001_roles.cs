﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class roles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "44fbd422-74e8-4436-963e-7e571e2a16a9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "3b810e2e-8d10-4f29-ab01-3c1d4fffec9b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "68f0b409-8d3f-4515-b7d8-8a24e4a32bf4");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "NormalizedUserName", "PasswordHash", "SecurityStamp", "UserName" },
                values: new object[] { "5d1a4f13-f5d3-4d2b-95a6-c396b28fd8ac", "ADMIN@GAPINSURANCES.COM", "AQAAAAEAACcQAAAAEIAIv3iTQpOUySQie75qiyX3zEhL0bQHAeBs/yZq++v5z32msHnlV8a1VhlNYIphEA==", "368de413-14c6-4728-a086-8acaa2adb6ea", "admin@gapinsurances.com" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "F1CC18F8-921F-4C24-9C65-453D30D2AAA8",
                columns: new[] { "ConcurrencyStamp", "NormalizedUserName", "PasswordHash", "SecurityStamp", "UserName" },
                values: new object[] { "e5cff623-570d-43cd-a4eb-b67a2600ee8c", "EMPLOYEE@GAPINSURANCES.COM", "AQAAAAEAACcQAAAAEA3452amt3eL5n0mos2sMnixJCQ9PlH82Xc4a9kZfedT82ljxwwSho+iObYkXpc3Jw==", "8d11082b-3f47-4d46-b130-881b0037b11c", "employee@gapinsurances.com" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "87947e34-4de8-42d9-85aa-50f5f860176a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "fc44941f-ced9-4793-ad97-3633ef9c44ac");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "b51bcc88-63ef-454e-afff-f6cb4f8642c9");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "Discriminator", "NormalizedUserName", "PasswordHash", "SecurityStamp", "UserName" },
                values: new object[] { "79a1b701-1c12-46d7-8a67-3d9bf0e7c5ee", "ApplicationUser", "ADMIN", "AQAAAAEAACcQAAAAENbvYZSe3jYn1k1HxZPkL6Bauz5r7YJgCd1hc3Q3Xt1ktpSIiLZEtmSKGJ4U/po/PQ==", "00000000-0000-0000-0000-000000000000", "admin" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "F1CC18F8-921F-4C24-9C65-453D30D2AAA8",
                columns: new[] { "ConcurrencyStamp", "Discriminator", "NormalizedUserName", "PasswordHash", "SecurityStamp", "UserName" },
                values: new object[] { "ac319db1-6fe0-49d4-82f6-d847d6c34382", "ApplicationUser", "EMPLOYEE", "AQAAAAEAACcQAAAAEKYHJbVNgSXMUHZbim5qHjrkKbIxsk8kf72c1C3nmK5koN0FFqyiXy/n6RXqG+vfUg==", "00000000-0000-0000-0000-000000000000", "employee" });
        }
    }
}
