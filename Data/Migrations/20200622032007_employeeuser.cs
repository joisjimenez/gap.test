﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class employeeuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "87947e34-4de8-42d9-85aa-50f5f860176a", "EMPLOYEE" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "fc44941f-ced9-4793-ad97-3633ef9c44ac");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "b51bcc88-63ef-454e-afff-f6cb4f8642c9", "GUEST" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "79a1b701-1c12-46d7-8a67-3d9bf0e7c5ee", "AQAAAAEAACcQAAAAENbvYZSe3jYn1k1HxZPkL6Bauz5r7YJgCd1hc3Q3Xt1ktpSIiLZEtmSKGJ4U/po/PQ==" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "F1CC18F8-921F-4C24-9C65-453D30D2AAA8", 0, "ac319db1-6fe0-49d4-82f6-d847d6c34382", "ApplicationUser", "employee@gapinsurances.com", true, false, null, "EMPLOYEE@GAPINSURANCES.COM", "EMPLOYEE", "AQAAAAEAACcQAAAAEKYHJbVNgSXMUHZbim5qHjrkKbIxsk8kf72c1C3nmK5koN0FFqyiXy/n6RXqG+vfUg==", null, true, "00000000-0000-0000-0000-000000000000", false, "employee" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "F1CC18F8-921F-4C24-9C65-453D30D2AAA8", "41D6BB0D-91E7-475A-A36E-A8B79F212FA7" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "F1CC18F8-921F-4C24-9C65-453D30D2AAA8", "41D6BB0D-91E7-475A-A36E-A8B79F212FA7" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "F1CC18F8-921F-4C24-9C65-453D30D2AAA8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "d5a606e8-9eff-497c-9050-ea7428bfbe4f", "Employee" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "5dfbca01-79f9-4c08-9bee-6f547671a2c9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "5545d624-12d8-4986-b4a6-47369799be00", "Guest" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "203364c3-7768-4150-b67e-7e8236857105", "AQAAAAEAACcQAAAAEDzth+FQkAkqMcU56HGbdfbEiVbN7EGZv78DJD9C4qzxDJGhA0/zPGvPA37+7kPWxw==" });
        }
    }
}
