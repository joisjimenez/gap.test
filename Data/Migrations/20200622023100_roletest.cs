﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class roletest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", "5A06EB99-281E-42F6-BF30-3775418F6851" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "5A06EB99-281E-42F6-BF30-3775418F6851", "ea446761-4182-4857-a35a-243c61fadb8c", "Administrator", "ADMINISTRATOR" },
                    { "41D6BB0D-91E7-475A-A36E-A8B79F212FA7", "ad7a7951-e6ba-47ea-acf8-11412bb1f90e", "Employee", "Employee" },
                    { "80FC19C3-5E7D-427B-8840-CD5A3ADAE846", "4805807a-a145-461c-bbfa-cf1ef3dc48fb", "Guest", "Guest" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", 0, "3d7e510f-2fcf-47cb-b3fc-29a482810a23", "ApplicationUser", "admin@gapinsurances.com", true, false, null, "ADMIN@GAPINSURANCES.COM", "ADMIN", "AQAAAAEAACcQAAAAEEyDVKhOPYmCFbwrV8VSbiWvou0IgGqWMustInabLGHYDn1Mqm7qfqC0mZXp6UKb8A==", null, true, "00000000-0000-0000-0000-000000000000", false, "admin" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", "5A06EB99-281E-42F6-BF30-3775418F6851" });
        }
    }
}
