﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class insuranceNameAndDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "Insurances",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "Insurances",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "ad7a7951-e6ba-47ea-acf8-11412bb1f90e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "ea446761-4182-4857-a35a-243c61fadb8c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "4805807a-a145-461c-bbfa-cf1ef3dc48fb");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3d7e510f-2fcf-47cb-b3fc-29a482810a23", "AQAAAAEAACcQAAAAEEyDVKhOPYmCFbwrV8VSbiWvou0IgGqWMustInabLGHYDn1Mqm7qfqC0mZXp6UKb8A==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description",
                table: "Insurances");

            migrationBuilder.DropColumn(
                name: "name",
                table: "Insurances");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "66d2265b-6c54-406b-b78c-f30dcb94de21");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "039bca01-4ef4-458d-8689-b794da4dd14b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "1c1d3a92-8612-4642-a9e7-04e88798577f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "367e6c38-56aa-4fda-9dc0-4a5f2eab9c6e", "AQAAAAEAACcQAAAAEMG6jplLciaMDrOjnMetYTpBzTyKCPC5olWetAKmV6MRPhf4Su4VhQ51tF6tgl24gg==" });
        }
    }
}
