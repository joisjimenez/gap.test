﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Beneficiaries",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    id_number = table.Column<string>(nullable: false),
                    full_name = table.Column<string>(nullable: true),
                    birth_date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beneficiaries", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Coverages",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    enabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coverages", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "InsuranceTypes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    risk_type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuranceTypes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Insurances",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    start_date = table.Column<DateTime>(nullable: false),
                    period = table.Column<int>(nullable: false),
                    total_price = table.Column<double>(nullable: false),
                    unsuranceTypeid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Insurances", x => x.id);
                    table.ForeignKey(
                        name: "FK_Insurances_InsuranceTypes_unsuranceTypeid",
                        column: x => x.unsuranceTypeid,
                        principalTable: "InsuranceTypes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BeneficiaryInsurances",
                columns: table => new
                {
                    insuranceId = table.Column<Guid>(nullable: false),
                    beneficiaryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BeneficiaryInsurances", x => new { x.beneficiaryId, x.insuranceId });
                    table.ForeignKey(
                        name: "FK_BeneficiaryInsurances_Beneficiaries_beneficiaryId",
                        column: x => x.beneficiaryId,
                        principalTable: "Beneficiaries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BeneficiaryInsurances_Insurances_insuranceId",
                        column: x => x.insuranceId,
                        principalTable: "Insurances",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoverageInsurances",
                columns: table => new
                {
                    insuranceId = table.Column<Guid>(nullable: false),
                    coverageId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoverageInsurances", x => new { x.coverageId, x.insuranceId });
                    table.ForeignKey(
                        name: "FK_CoverageInsurances_Coverages_coverageId",
                        column: x => x.coverageId,
                        principalTable: "Coverages",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoverageInsurances_Insurances_insuranceId",
                        column: x => x.insuranceId,
                        principalTable: "Insurances",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "5A06EB99-281E-42F6-BF30-3775418F6851", "6c54d030-2d71-48ca-bd2d-591c7d6c914e", "Administrator", "ADMINISTRATOR" },
                    { "41D6BB0D-91E7-475A-A36E-A8B79F212FA7", "781da5ad-4026-4849-93c4-4889d2e4fdff", "Employee", "Employee" },
                    { "80FC19C3-5E7D-427B-8840-CD5A3ADAE846", "80f85870-5f00-4a20-ae52-ff99b296ffff", "Guest", "Guest" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", 0, "fdb28ccf-6d49-4abf-9da3-5fffa3ad4fea", "ApplicationUser", "Admin@Admin.com", true, false, null, "ADMIN@ADMIN.COM", "MASTERADMIN", "AQAAAAEAACcQAAAAEKO0iCDX3S4/dWQZiLlsLRcHoDGDwQt00q/9fhA/ZiAfQnioLpR5rEXelm72N55aLA==", "XXXXXXXXXXXXX", true, "00000000-0000-0000-0000-000000000000", false, "masteradmin" });

            migrationBuilder.InsertData(
                table: "Coverages",
                columns: new[] { "id", "enabled", "name" },
                values: new object[,]
                {
                    { 1, true, "Terremoto" },
                    { 2, true, "Incendio" },
                    { 3, true, "Robo" },
                    { 4, true, "Pérdida" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", "5A06EB99-281E-42F6-BF30-3775418F6851" });

            migrationBuilder.CreateIndex(
                name: "IX_BeneficiaryInsurances_insuranceId",
                table: "BeneficiaryInsurances",
                column: "insuranceId");

            migrationBuilder.CreateIndex(
                name: "IX_CoverageInsurances_insuranceId",
                table: "CoverageInsurances",
                column: "insuranceId");

            migrationBuilder.CreateIndex(
                name: "IX_Insurances_unsuranceTypeid",
                table: "Insurances",
                column: "unsuranceTypeid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BeneficiaryInsurances");

            migrationBuilder.DropTable(
                name: "CoverageInsurances");

            migrationBuilder.DropTable(
                name: "Beneficiaries");

            migrationBuilder.DropTable(
                name: "Coverages");

            migrationBuilder.DropTable(
                name: "Insurances");

            migrationBuilder.DropTable(
                name: "InsuranceTypes");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", "5A06EB99-281E-42F6-BF30-3775418F6851" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");
        }
    }
}
