﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class CoveragePercent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "coverage_percent",
                table: "Insurances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "5b11cfd0-a9aa-430d-882a-44eb655e187b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "3229d541-5d20-474c-a771-db52f5323267");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "ad8aaba4-5956-4dda-87ec-181cc4f22b7b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "Email", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "UserName" },
                values: new object[] { "cc828707-b922-4be6-b65e-6813ce08eb8a", "admin@gapinsurances.com", "ADMIN@GAPINSURANCES.COM", "ADMIN", "AQAAAAEAACcQAAAAEPfmE+q1hbMbNm3DkmSPROBanCJYkTTTqGl/iEEO7XsiAoWZXX9YeWO8VPpgdjs/cA==", null, "admin" });

            migrationBuilder.InsertData(
                table: "InsuranceTypes",
                columns: new[] { "id", "risk_type" },
                values: new object[,]
                {
                    { 1, "Bajo" },
                    { 2, "Medio" },
                    { 3, "Medio-alto" },
                    { 4, "Alto" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "InsuranceTypes",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "InsuranceTypes",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "InsuranceTypes",
                keyColumn: "id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "InsuranceTypes",
                keyColumn: "id",
                keyValue: 4);

            migrationBuilder.DropColumn(
                name: "coverage_percent",
                table: "Insurances");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7",
                column: "ConcurrencyStamp",
                value: "781da5ad-4026-4849-93c4-4889d2e4fdff");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851",
                column: "ConcurrencyStamp",
                value: "6c54d030-2d71-48ca-bd2d-591c7d6c914e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846",
                column: "ConcurrencyStamp",
                value: "80f85870-5f00-4a20-ae52-ff99b296ffff");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8",
                columns: new[] { "ConcurrencyStamp", "Email", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "UserName" },
                values: new object[] { "fdb28ccf-6d49-4abf-9da3-5fffa3ad4fea", "Admin@Admin.com", "ADMIN@ADMIN.COM", "MASTERADMIN", "AQAAAAEAACcQAAAAEKO0iCDX3S4/dWQZiLlsLRcHoDGDwQt00q/9fhA/ZiAfQnioLpR5rEXelm72N55aLA==", "XXXXXXXXXXXXX", "masteradmin" });
        }
    }
}
