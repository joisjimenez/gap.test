﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GAP.InsuranceTest.Data.Migrations
{
    public partial class userAdmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "5A06EB99-281E-42F6-BF30-3775418F6851", "5dfbca01-79f9-4c08-9bee-6f547671a2c9", "Administrator", "ADMINISTRATOR" },
                    { "41D6BB0D-91E7-475A-A36E-A8B79F212FA7", "d5a606e8-9eff-497c-9050-ea7428bfbe4f", "Employee", "Employee" },
                    { "80FC19C3-5E7D-427B-8840-CD5A3ADAE846", "5545d624-12d8-4986-b4a6-47369799be00", "Guest", "Guest" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", 0, "203364c3-7768-4150-b67e-7e8236857105", "ApplicationUser", "admin@gapinsurances.com", true, false, null, "ADMIN@GAPINSURANCES.COM", "ADMIN", "AQAAAAEAACcQAAAAEDzth+FQkAkqMcU56HGbdfbEiVbN7EGZv78DJD9C4qzxDJGhA0/zPGvPA37+7kPWxw==", null, true, "00000000-0000-0000-0000-000000000000", false, "admin" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", "5A06EB99-281E-42F6-BF30-3775418F6851" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41D6BB0D-91E7-475A-A36E-A8B79F212FA7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80FC19C3-5E7D-427B-8840-CD5A3ADAE846");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8", "5A06EB99-281E-42F6-BF30-3775418F6851" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5A06EB99-281E-42F6-BF30-3775418F6851");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");
        }
    }
}
