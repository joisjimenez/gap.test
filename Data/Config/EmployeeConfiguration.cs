﻿using GAP.InsuranceTest.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAP.InsuranceTest.Data.Config
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        private const string adminId = "F1CC18F8-921F-4C24-9C65-453D30D2AAA8";
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {

            var admin = new ApplicationUser
            {
                Id = adminId,
                UserName = "employee@gapinsurances.com",
                NormalizedUserName = "EMPLOYEE@GAPINSURANCES.COM",
                Email = "employee@gapinsurances.com",
                NormalizedEmail = "EMPLOYEE@GAPINSURANCES.COM",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            admin.PasswordHash = PassGenerate(admin);

            builder.HasData(admin);
        }
       

        public string PassGenerate(ApplicationUser user)
        {
            var passHash = new PasswordHasher<ApplicationUser>();
            return passHash.HashPassword(user, "password");
        }
    }
}
