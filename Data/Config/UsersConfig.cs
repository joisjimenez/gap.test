﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAP.InsuranceTest.Data.Config
{
    public class UsersConfig : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        private const string adminUserId = "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8";
        private const string adminRoleId = "5A06EB99-281E-42F6-BF30-3775418F6851";
        private const string employeeUserId = "F1CC18F8-921F-4C24-9C65-453D30D2AAA8";
        private const string employeeRoleId = "41D6BB0D-91E7-475A-A36E-A8B79F212FA7";

        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {

            builder.HasData(
                new IdentityUserRole<string>
                {
                    RoleId = adminRoleId,
                    UserId = adminUserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = employeeRoleId,
                    UserId = employeeUserId
                }
            );
        }
    }
}
