﻿using GAP.InsuranceTest.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAP.InsuranceTest.Data.Config
{
    public class AdminConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        private const string adminId = "8AC6273B-E533-4606-90A5-7A1C5EB1ABC8";
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {

            var admin = new ApplicationUser
            {
                Id = adminId,
                UserName = "admin@gapinsurances.com",
                NormalizedUserName = "ADMIN@GAPINSURANCES.COM",
                Email = "admin@gapinsurances.com",
                NormalizedEmail = "ADMIN@GAPINSURANCES.COM",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            admin.PasswordHash = PassGenerate(admin);

            builder.HasData(admin);
        }

        public string PassGenerate(ApplicationUser user)
        {
            var passHash = new PasswordHasher<ApplicationUser>();
            return passHash.HashPassword(user, "password");
        }
    }
}
