﻿using GAP.InsuranceTest.Core.Entities;
using GAP.InsuranceTest.Data.Interfaces;

namespace GAP.InsuranceTest.Data.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _dbContext;
        private BaseRepository<Insurance> _insurances;
        private BaseRepository<Beneficiary> _beneficiaries;
        private BaseRepository<Coverage> _coverages;
        private BaseRepository<BeneficiaryInsurance> _beneficiaryInsurances;
        private BaseRepository<CoverageInsurance> _coverageInsurances;
        private BaseRepository<InsuranceRiskType> _insuranceRiskTypes;


        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IRepository<Insurance> Insurances => _insurances ?? (_insurances = new BaseRepository<Insurance>(_dbContext));
        public IRepository<Beneficiary> Beneficiaries => _beneficiaries ?? (_beneficiaries = new BaseRepository<Beneficiary>(_dbContext));
        public IRepository<Coverage> Coverages => _coverages ?? (_coverages = new BaseRepository<Coverage>(_dbContext));
        public IRepository<BeneficiaryInsurance> BeneficiaryInsurances => _beneficiaryInsurances ?? (_beneficiaryInsurances = new BaseRepository<BeneficiaryInsurance>(_dbContext));
        public IRepository<CoverageInsurance> CoverageInsurances => _coverageInsurances ?? (_coverageInsurances = new BaseRepository<CoverageInsurance>(_dbContext));
        public IRepository<InsuranceRiskType> InsuranceRiskTypes => _insuranceRiskTypes ?? (_insuranceRiskTypes = new BaseRepository<InsuranceRiskType>(_dbContext));

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
