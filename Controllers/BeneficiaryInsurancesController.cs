﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GAP.InsuranceTest.Core.Entities;
using GAP.InsuranceTest.Data;
using GAP.InsuranceTest.Data.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace GAP.InsuranceTest.Controllers
{
    [Authorize(Roles ="Administrator")]
    public class BeneficiaryInsurancesController : Controller
    {
        private readonly IUnitOfWork _repository;

        public BeneficiaryInsurancesController(IUnitOfWork repository)
        {
            _repository = repository;
        }

        // GET: BeneficiaryInsurances
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _repository.BeneficiaryInsurances.Get(null, null, "beneficiary,insurance");
            return View(applicationDbContext);
        }

        // GET: BeneficiaryInsurances/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beneficiaryInsurance = _repository.BeneficiaryInsurances.Get(x => x.beneficiaryId == id, null, "beneficiary,insurance").FirstOrDefault();
            if (beneficiaryInsurance == null)
            {
                return NotFound();
            }

            return View(beneficiaryInsurance);
        }

        // GET: BeneficiaryInsurances/Create
        public IActionResult Create()
        {
            ViewData["beneficiaryId"] = new SelectList(_repository.Beneficiaries.Get(), "id", "full_name");
            ViewData["insuranceId"] = new SelectList(_repository.Insurances.Get(), "id", "name");
            return View();
        }

        // POST: BeneficiaryInsurances/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("insuranceId,beneficiaryId")] BeneficiaryInsurance beneficiaryInsurance)
        {
            if (ModelState.IsValid)
            {
                //beneficiaryInsurance.beneficiaryId = Guid.NewGuid();
                _repository.BeneficiaryInsurances.Insert(beneficiaryInsurance);
                _repository.Commit();
                return RedirectToAction(nameof(Index));
            }
            ViewData["beneficiaryId"] = new SelectList(_repository.Beneficiaries.Get(), "id", "full_name");
            ViewData["insuranceId"] = new SelectList(_repository.Insurances.Get(), "id", "name");
            return View(beneficiaryInsurance);
        }


        // GET: BeneficiaryInsurances/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beneficiaryInsurance = _repository.BeneficiaryInsurances.Get(x => x.beneficiaryId == id, null, "beneficiary,insurance").FirstOrDefault();
            if (beneficiaryInsurance == null)
            {
                return NotFound();
            }

            return View(beneficiaryInsurance);
        }

        // POST: BeneficiaryInsurances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var beneficiaryInsurance = _repository.BeneficiaryInsurances.GetByID(id);
            _repository.BeneficiaryInsurances.Delete(beneficiaryInsurance);
            _repository.Commit();
            return RedirectToAction(nameof(Index));
        }

     
    }
}
