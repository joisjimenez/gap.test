﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GAP.InsuranceTest.Core.Entities;
using GAP.InsuranceTest.Data;
using GAP.InsuranceTest.Data.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace GAP.InsuranceTest.Controllers
{
    [Authorize(Roles = "Administrator,Employee")]
    public class BeneficiariesController : Controller
    {
        private readonly IUnitOfWork _repository;

        public BeneficiariesController(IUnitOfWork repository)
        {
            _repository = repository;
        }

        // GET: Beneficiaries
        public async Task<IActionResult> Index()
        {
            return View(_repository.Beneficiaries.Get());
        }

        // GET: Beneficiaries/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beneficiary = _repository.Beneficiaries.GetByID(id);
            if (beneficiary == null)
            {
                return NotFound();
            }

            return View(beneficiary);
        }

        // GET: Beneficiaries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Beneficiaries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,id_number,full_name,birth_date")] Beneficiary beneficiary)
        {
            if (ModelState.IsValid)
            {
                beneficiary.id = Guid.NewGuid();
                _repository.Beneficiaries.Insert(beneficiary);
                _repository.Commit();
                return RedirectToAction(nameof(Index));
            }
            return View(beneficiary);
        }

        // GET: Beneficiaries/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beneficiary = _repository.Beneficiaries.GetByID(id);
            if (beneficiary == null)
            {
                return NotFound();
            }
            return View(beneficiary);
        }

        // POST: Beneficiaries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("id,id_number,full_name,birth_date")] Beneficiary beneficiary)
        {
            if (id != beneficiary.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Beneficiaries.Update(beneficiary);
                    _repository.Commit();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BeneficiaryExists(beneficiary.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(beneficiary);
        }

        // GET: Beneficiaries/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beneficiary = _repository.Beneficiaries.GetByID(id);
            if (beneficiary == null)
            {
                return NotFound();
            }

            return View(beneficiary);
        }

        // POST: Beneficiaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var beneficiary = _repository.Beneficiaries.GetByID(id);
            _repository.Beneficiaries.Delete(beneficiary);
            _repository.Commit();
            return RedirectToAction(nameof(Index));
        }

        private bool BeneficiaryExists(Guid id)
        {
            return _repository.Beneficiaries.GetByID(id) != null;
        }
    }
}
