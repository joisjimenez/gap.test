﻿using GAP.InsuranceTest.Core.Entities;
using GAP.InsuranceTest.Data;
using GAP.InsuranceTest.Data.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAP.InsuranceTest.Controllers
{
    [Authorize(Roles ="Administrator,Employee")]
    public class InsurancesController : Controller
    {
        private readonly IUnitOfWork _repository;

        public InsurancesController(IUnitOfWork repository)
        {
            _repository = repository;
        }

        // GET: Insurances
        public async Task<IActionResult> Index()
        {
            return View(_repository.Insurances.Get());
        }

        // GET: Insurances/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var insurance = _repository.Insurances.GetByID(id);
           
            if (insurance == null)
            {
                return NotFound();
            }

            return View(insurance);
        }

        // GET: Insurances/Create
        public IActionResult Create()
        {
            Insurance model = new Insurance();
            ViewBag.coverageList = _repository.Coverages.Get();
            ViewBag.rtlist = _repository.InsuranceRiskTypes.Get();

            return View();
        }

        // POST: Insurances/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,description,,start_date,period,total_price,insuranceRiskTypeid,coverage_percent")] Insurance insurance, string[] coverages)
        {
            if (ModelState.IsValid)
            {
                insurance.id = Guid.NewGuid();
                _repository.Insurances.Insert(insurance);

                // add coverages
                for (int i = 0; i < coverages.Length; i++)
                {
                    int covId;
                    if (int.TryParse(coverages[i], out covId))
                    {
                        CoverageInsurance entity = new CoverageInsurance()
                        {
                            coverageId = covId,
                            insuranceId = insurance.id
                        };

                        _repository.CoverageInsurances.Insert(entity);
                    }
                }

                _repository.Commit();
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            coverages = null;
            ViewBag.coverageList = null;

            ViewBag.coverageList = _repository.Coverages.Get();

            ViewBag.rtlist = _repository.InsuranceRiskTypes.Get().ToList();
            return View(insurance);
        }

        // GET: Insurances/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var insurance = _repository.Insurances.GetByID(id);
            if (insurance == null)
            {
                return NotFound();
            }

            ViewBag.beneficiaries = this.GetBeneficiariesByInsurance(id);
            ViewBag.rtlist = _repository.InsuranceRiskTypes.Get();

            return View(insurance);
        }

        // POST: Insurances/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("name,description,start_date,period,total_price,insuranceRiskTypeid,coverage_percent")] Insurance insurance)
        {
            if (id != insurance.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Insurances.Update(insurance);
                    _repository.Commit();
                }
                catch (DbUpdateConcurrencyException)
                {
                    //if (!InsuranceExists(insurance.id))
                    //{
                    //    return NotFound();
                    //}
                    //else
                    //{
                    throw;
                    //}
                }
                return RedirectToAction(nameof(Index));
            }
            return View(insurance);
        }

        // GET: Insurances/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var insurance = _repository.Insurances.GetByID(id);
            if (insurance == null)
            {
                return NotFound();
            }

            return View(insurance);
        }

        // POST: Insurances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var insurance = _repository.Insurances.GetByID(id);
            _repository.Insurances.Delete(insurance);
            _repository.Commit();
            return RedirectToAction(nameof(Index));
        }


        public async Task<IActionResult> Associate(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var list = _repository.CoverageInsurances.Get(x => x.insuranceId == id);
            if (list == null)
            {
                return NotFound();
            }

            ViewBag.beneficiaries = _repository.BeneficiaryInsurances.Get().ToList();
            return RedirectToAction(nameof(Index));
        }


        [HttpPost, ActionName("Associate")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssociateConfirmed(FormCollection form)
        {
            //if (insuranceId == null)
            //{
            //    return NotFound();
            //}

            //var list = _repository.CoverageInsurances.Get(x => x.insuranceId == insuranceId);
            //if (list == null)
            //{
            //    return NotFound();
            //}

            return RedirectToAction(nameof(Index));

        }

        private List<Coverage> GetInsuranceCoverages(Guid? insuranceId)
        {
            if (insuranceId == null)
            {
                return null;
            }

            var list = _repository.CoverageInsurances.Get(x => x.insuranceId == insuranceId, null, "coverages").Select(x => x.coverage).ToList();
            return list;
        }


        private List<Beneficiary> GetBeneficiariesByInsurance(Guid? insuranceId)
        {
            if (insuranceId == null)
            {
                return null;
            }

            var list = _repository.BeneficiaryInsurances.Get(x => x.insuranceId == insuranceId, null, "beneficiary").Select(x => x.beneficiary).ToList();
            return list;
        }
    }
}
