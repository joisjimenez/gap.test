# GAP Insurances Test #

**Author: Joisman Jim�nez**

### Summary ###

Knowledge Test Project for GAP. Developed with ASP.NET Core (3.1) MVC and WebAPI, EntityFrameworkCore, Identity and NUnit. This project uses *code-first* approach, so it's needed to use `Update-Database` command before launch the application.

### Logins ###
* user: admin@gapinsurances.com (Role admin)
password: password

* user: employee@gapinsurances.com (Role employee)
password: password

### Packages ###

#### GAP.InsuranceTest ####

* Microsoft.EntityFrameworkCore.Design
* Microsoft.EntityFrameworkCore.Tools
* Microsoft.EntityFrameworkCore.SqlServer
* Newtonsoft.Json

#### Test/GAP.InsuranceTest.Testing ####

* Microsoft.AspNetCore.TestHost
* NUnit

### References ###

*	NUnit Entity Framework integration test dependency injection issue with .NET Core 
  https://stackoverflow.com/a/49351242/2266869
*	.Net Core MVC With Entity Framework Core Using Dependency Injection And Repository
  https://www.c-sharpcorner.com/blogs/net-core-mvc-with-entity-framework-core-using-dependency-injection-and-repository
*   IValidatableObject.Validate(ValidationContext) Method
https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations.ivalidatableobject.validate?view=netcore-3.1#System_ComponentModel_DataAnnotations_IValidatableObject_Validate_System_ComponentModel_DataAnnotations_ValidationContext_

