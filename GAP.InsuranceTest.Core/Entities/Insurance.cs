﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GAP.InsuranceTest.Core.Entities
{
    public class Insurance : IValidatableObject
    {

        [Required]
        [Display(Name = "Fecha de Inicio")]
        public Guid id { get; set; }

        [Required]
        [Display(Name ="Nombre")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        public string description { get; set; }

        [Required]
        [Display(Name = "Fecha de Inicio")]
        public DateTime start_date { get; set; }

        [Required]
        [Display(Name = "Periodo de cobertura (meses)")]
        public int period { get; set; }

        [Required]
        [Display(Name = "Valor Total")]
        [DataType(DataType.Currency)]
        public double total_price { get; set; }

        [Required]
        [Display(Name = "% de Cobertura")]
        public double coverage_percent { get; set; }

        [Display(Name = "Tipo de Riesgo")]
        [ForeignKey("insuranceRiskTypeid")]
        public InsuranceRiskType insuranceRiskType { get; set; }
        public int insuranceRiskTypeid { get; set; }
        public List<CoverageInsurance> coverageInsurances { get; set; }
        public List<BeneficiaryInsurance> beneficiaryInsurances { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (insuranceRiskTypeid == (int)Enums.RiskTypes.Alto && coverage_percent > 50)
                yield return new ValidationResult("No se puede asegurar más del 50% cuando el riesgo es alto.");
        }
    }
}
