﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.InsuranceTest.Core.Entities
{
    public class Coverage
    {

        public int id { get; set; }
        public string name { get; set; }
        public bool enabled { get; set; }

        public List<CoverageInsurance> coverageInsurances { get; set; }
    }
}
