﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GAP.InsuranceTest.Core.Entities
{
   public class Beneficiary
    {

        public Beneficiary()
        {
        }

        public Guid id { get; set; }
        [Required]
        public string id_number { get; set; }
        public string full_name { get; set; }
        public DateTime birth_date { get; set; }
        public List<BeneficiaryInsurance> beneficiaryInsurances { get; set; }
    }
}
