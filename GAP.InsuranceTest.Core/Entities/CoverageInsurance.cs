﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.InsuranceTest.Core.Entities
{
    public class CoverageInsurance
    {
        public Guid insuranceId { get; set; }
        public Insurance insurance { get; set; }
        public int coverageId { get; set; }
        public Coverage coverage { get; set; }
    }
}
