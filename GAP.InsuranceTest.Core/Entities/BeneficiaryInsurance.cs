﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.InsuranceTest.Core.Entities
{
    public class BeneficiaryInsurance
    {
        public Guid insuranceId { get; set; }
        public Insurance insurance { get; set; }
        public Guid beneficiaryId { get; set; }
        public Beneficiary beneficiary { get; set; }
    }
}
