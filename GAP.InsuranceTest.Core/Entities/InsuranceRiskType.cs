﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.InsuranceTest.Core.Entities
{
    public class InsuranceRiskType
    {
        public int id { get; set; }
        public string risk_type { get; set; }

        //public virtual ICollection<Insurance> Insurance { get; set; }
    }
}
