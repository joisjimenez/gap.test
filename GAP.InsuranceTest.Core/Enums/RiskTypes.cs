﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.InsuranceTest.Core.Enums
{
    public enum RiskTypes
    {
        Bajo =1 ,
        Medio = 2,
        MedioAlto = 3,
        Alto = 4
    }
}
