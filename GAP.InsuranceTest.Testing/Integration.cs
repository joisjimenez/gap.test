using GAP.InsuranceTest.Core.Entities;
using GAP.InsuranceTest.Data.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Net.Http;
using System.Threading.Tasks;

namespace GAP.InsuranceTest.Testing
{
    public class Tests
    {
        private TestServer _server;
        private HttpClient _client;
        private readonly IUnitOfWork _repository;

        private IConfiguration _configuration;

    
        [OneTimeSetUp]
        public void Setup()
        {
            _configuration = new ConfigurationBuilder()
                 .AddJsonFile("appsettings.json")
                 .Build();

            //
            this._server = new TestServer(new WebHostBuilder()
                .UseConfiguration(_configuration)
                .UseStartup<Startup>());
            this._client = _server.CreateClient();
      
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _server = null;
            _client = null;
        }

        [Test]
        public async Task TestListInsurances()
        {
            // Act
            var response = await _client.GetAsync("/insurances");
            response.EnsureSuccessStatusCode();
            
            var responseString = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.Equals("Hello World!", responseString);
        }

        [Test]
        public async Task TestSaveCoveragesInsurance()
        {

            CoverageInsurance obj = new CoverageInsurance();
            obj.coverageId = 1;

            //_client.PostAsync("/insurances/associate", new { 
            //    "": 

            //})



            var insurance = _repository.InsuranceRiskTypes.Get();
        }
    }
}